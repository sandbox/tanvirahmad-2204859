INTRODUCTION
------------
The Limit publishing Options provides an interface to limit 
the publishing option like "Promote to Front" or "Sticky" to roles 
to a specific number per day.

INSTALLATION
------------
* Install as usual, 
see https://drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
-------------
* Configure user permissions in Administration >> People >> Permissions:
  - The Limit Publishing Options will not be accessed unless this permission is
    granted.
  - Access Limit Publishing Options
    Users in roles with the "Administer limit publishing options" permission 
will see 
    Limit Publishing Options at Administration >> Configuration >> Content 
authoring >> Limit Publishing Options.
