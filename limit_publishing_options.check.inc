<?php

/**
 * @file
 * Provides the custom functions available.
 */

/**
 * This will check if user exceeds the limit set in the form.
 *
 * @param string $publication_op
 *   Publication option, sticky or promote.
 */
function limit_publishing_options_check_per_user($publication_op) {
  global $user;
  $result = db_select('limit_publishing_options', 'n')
    ->fields('n')
    ->condition('uid', $user->uid, '=')
    ->condition('publication_op', $publication_op, '=')
    ->execute();
  $num_of_results = $result->rowCount();
  return $num_of_results;
}

/**
 * This will check if role of the current user is one of the selected.
 */
function limit_publishing_options_check_per_role() {
  global $user;
  $filterd_roles = array_filter(variable_get('limit_publishing_options_roles'));
  foreach ($filterd_roles as $key) {
    if (array_key_exists($key, $user->roles)) {
      return $key;
    }
  }
  return NULL;
}

/**
 * This will display the ajax message.
 */
function limit_publishing_options_po_notification() {
  $commands = array();
  $commands[] = ajax_command_prepend('div#ajax-status-messages-wrapper', drupal_set_message(variable_get('limit_publishing_options_notification')));
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * This will add publishing option into table either sticky or promoted.
 *
 * @param string $publishing_option
 *   Publication option, sticky or promote.
 */
function limit_publishing_options_add_option($publishing_option) {
  global $user;
  $date = date("Y-m-d", REQUEST_TIME);
  db_insert('limit_publishing_options')
    ->fields(array(
      'uid' => $user->uid,
      'date' => $date,
      'publication_op' => $publishing_option,
    ))
    ->execute();
}
